# March

March is a continuous integration server.

March is in pre-alpha status.

## Development

### Architecture

### Testing

## Alpha roadmap

The following are a list of goals to implement, not necessarily in order.

### Command line interface
A simple command line interface that can build and test Maven projects. Prints progress information.

    $ march
    Building...  done.
    Testing...   pass.
    Packaging... done.
    Verifying... pass.
    Completed in 1m 28s.

### Errors
Reports compile and test errors in a human readable way. May report test failures as they happen.

### Simple persistence
Keeps track of projects. Persists to disk, does not stay memory resident yet.

    foo$ march add
    Added ’foo’.
    $ march list
    foo: /path/to/foo
    bar: ...
    $ march run
    foo:
       Building... done.
       ...
    bar:
        Building...

Persists state to e.g. `~/.march.json`.

### Virtual machines
Runs each build in a dedicated VM, possibly in the cloud. If too much overhead, may use Linux Containers (LXC).

### Working space
Clones repositories to working space instead of building in place.

### Persistent working space
Keeps a persistent working space. Recreates it if necessary.

### Resident daemon
Stays memory resident. Command line app communicates with the daemon.

### Web interface
A web client that communicates with the daemon via the same protocol as the command line client.

#### Radiator

### User management