package net.wolverian.march.builders;

import com.google.common.base.*;

import java.util.*;

public class MavenException extends RuntimeException {
	public MavenException(Throwable inner) {
		super("Error executing 'mvn verify'.", inner);
	}

	public MavenException(List<String> output) {
		super("Error executing 'mvn verify': " + Joiner.on('\n').join(output));
	}
}
