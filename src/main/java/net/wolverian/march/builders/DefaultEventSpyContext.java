package net.wolverian.march.builders;

import org.apache.maven.eventspy.*;

import java.util.*;

public class DefaultEventSpyContext implements EventSpy.Context {
    private final Map<String, Object> data = new HashMap<>();

    @Override
    public Map<String, Object> getData() {
        return data;
    }
}
