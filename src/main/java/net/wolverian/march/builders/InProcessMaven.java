package net.wolverian.march.builders;

import net.wolverian.march.*;
import org.apache.maven.eventspy.internal.*;
import org.apache.maven.execution.*;
import org.apache.maven.model.building.*;
import org.apache.maven.settings.building.*;
import org.codehaus.plexus.*;
import org.codehaus.plexus.classworlds.*;
import org.codehaus.plexus.component.repository.exception.*;
import org.codehaus.plexus.logging.*;
import org.jmock.example.announcer.*;
import org.sonatype.plexus.components.sec.dispatcher.*;

import java.nio.file.*;
import java.util.*;

import static java.util.Arrays.*;

public class InProcessMaven extends AbstractExecutionListener implements Maven {
    private Path path;
    private Announcer<BuildListener> listeners = Announcer.to(BuildListener.class);
    private ModelProcessor modelProcessor;
    private SettingsBuilder settingsBuilder;
    private DefaultSecDispatcher dispatcher;
    private MavenExecutionRequestPopulator executionRequestPopulator;
    private org.apache.maven.Maven maven;
    private Logger logger;
    private EventSpyDispatcher eventSpyDispatcher;
    private DefaultPlexusContainer container;
    private ClassWorld classWorld;
    private MavenExecutionRequest request;

    @Override
    public Maven in(Path path) {
        this.path = path;
        return this;
    }

    @Override
    public Maven withListener(BuildListener listener) {
        listeners.addListener(listener);
        return this;
    }

    @Override
    public void build() {
        request = new DefaultMavenExecutionRequest()
                .setBaseDirectory(path.toFile())
                .setGoals(asList("verify"))
                .setExecutionListener(this);
        classWorld = new ClassWorld("plexus.core", Thread.currentThread().getContextClassLoader());
        ContainerConfiguration configuration = new DefaultContainerConfiguration()
                .setClassWorld(classWorld)
                .setRealm(classWorld.getClassRealm("plexus.core"))
                .setName("maven");
        try {
            container = new DefaultPlexusContainer(configuration);
            container.setLookupRealm(null);
            Thread.currentThread().setContextClassLoader( container.getContainerRealm() );
            eventSpyDispatcher = container.lookup(EventSpyDispatcher.class);
            DefaultEventSpyContext eventSpyContext = new DefaultEventSpyContext();
            Map<String, Object> data = eventSpyContext.getData();
            data.put( "plexus", container);
//            data.put( "workingDirectory", cliRequest.workingDirectory );
//            data.put( "systemProperties", cliRequest.systemProperties );
//            data.put( "userProperties", cliRequest.userProperties );
//            data.put( "versionProperties", CLIReportingUtils.getBuildProperties() );
            eventSpyDispatcher.init(eventSpyContext);
            // refresh logger in case container got customized by spy
            logger = container.getLoggerManager().getLoggerForComponent(InProcessMaven.class.getName());
            maven = container.lookup(org.apache.maven.Maven.class);
            executionRequestPopulator = container.lookup(MavenExecutionRequestPopulator.class);
            modelProcessor = container.lookup(ModelProcessor.class);
            settingsBuilder = container.lookup(SettingsBuilder.class);
            dispatcher = (DefaultSecDispatcher) container.lookup(SecDispatcher.class, "maven");

            maven.execute(request);

            container.dispose();
        } catch (PlexusContainerException|ComponentLookupException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void projectStarted(ExecutionEvent event) {
        listeners.announce().buildStarted();
    }
}
