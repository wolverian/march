package net.wolverian.march.builders;

import com.google.common.io.*;
import net.wolverian.march.*;
import org.jmock.example.announcer.*;

import java.io.*;
import java.nio.file.*;

public class PosixMaven implements Maven {
	private final ProcessBuilder processBuilder = new ProcessBuilder("mvn", "verify");
    private final Announcer<BuildListener> announcer = Announcer.to(BuildListener.class);

	@Override
	public Maven in(Path path) {
		processBuilder.directory(path.toFile());
		return this;
	}

	@Override
	public void build() {
		try {
            announcer.announce().buildStarted();
			Process process = processBuilder.start();
			if (process.waitFor() != 0) {
				throw new MavenException(CharStreams.readLines(new InputStreamReader(process.getInputStream())));
			}
		} catch (InterruptedException | IOException e) {
			throw new MavenException(e);
		}
	}

    @Override
    public Maven withListener(BuildListener listener) {
        announcer.addListener(listener);
        return this;
    }
}
