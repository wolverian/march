package net.wolverian.march.builders;

import net.wolverian.march.*;

import java.nio.file.*;

public interface Maven {
	Maven in(Path path);
    Maven withListener(BuildListener listener);
    void build();
}
