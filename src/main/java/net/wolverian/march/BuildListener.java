package net.wolverian.march;

import java.util.*;

public interface BuildListener extends EventListener {
    void buildStarted();
}
