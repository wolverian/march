package net.wolverian.march;

import net.wolverian.march.builders.*;

public class CommandLine implements BuildListener {
    public static void main(String[] arguments) {
        new CommandLine().run();
    }

    private void run() {
        new PosixMaven().withListener(this).build();
    }

    @Override
    public void buildStarted() {
        System.out.println("Build started");
    }
}
