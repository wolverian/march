package net.wolverian.march.builders;

public class InProcessMavenIT extends MavenIT {
    @Override
    protected Maven getMaven() {
        return new InProcessMaven();
    }
}
