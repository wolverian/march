package net.wolverian.march.builders;

import net.wolverian.march.*;
import net.wolverian.march.fixtures.*;
import org.jmock.*;
import org.jmock.integration.junit4.*;
import org.junit.*;

import java.nio.file.*;

import static net.wolverian.march.Util.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public abstract class MavenIT {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();
    @Rule
    public Projects projects = new Projects();

    private final BuildListener listener = context.mock(BuildListener.class);

    private Path path = projects.createMavenProject();

    @Test
    public void shouldNotifyListenersWhenBuildStarted() {
        context.checking(new Expectations() {{
            oneOf(listener).buildStarted();
        }});

        getMaven().withListener(listener)
             .in(path)
             .build();
    }

    @Test
    public void shouldBuildIntoTarget() {
        getMaven().in(path).build();

        assertThat(builtFiles(path), hasItem(endsWith(".class")));
    }

    protected abstract Maven getMaven();
}
