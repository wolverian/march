package net.wolverian.march.builders;

public class PosixMavenIT extends MavenIT {
    @Override
    protected Maven getMaven() {
        return new PosixMaven();
    }
}
