package net.wolverian.march;

import com.google.common.io.*;

import java.io.*;
import java.nio.file.*;
import java.nio.file.Files;
import java.nio.file.attribute.*;
import java.util.*;

public final class Util {
	private Util() {}

	public static List<String> builtFiles(Path target) {
        return gatherFileNamesRecursivelyIn(target.resolve("target"));
	}

    private static List<String> gatherFileNamesRecursivelyIn(Path target) {
        final List<String> files = new ArrayList<>();

        try {
            Files.walkFileTree(target, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    files.add(file.getFileName().toString());
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return files;
    }

    static String textFrom(InputStream stream) {
        try {
            return CharStreams.toString(new InputStreamReader(stream));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
