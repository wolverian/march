package net.wolverian.march.fixtures;

import com.google.common.io.*;
import org.junit.*;
import org.junit.rules.*;

import java.net.*;
import java.nio.file.Files;
import java.nio.file.*;

public class Projects extends ExternalResource {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	public Path createMavenProject() {
		try {
			Path root = temporaryFolder.newFolder().toPath();
			Files.copy(resource("example-pom.xml"), root.resolve("pom.xml"));
			Path packageDirectory = Files.createDirectories(root.resolve("src/main/java/net/wolverian/example/"));
			Files.copy(resource("Foo.java"), packageDirectory.resolve("Foo.java"));
			return root;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private Path resource(String name) throws URISyntaxException {
		return Paths.get(Resources.getResource(name).toURI());
	}
}
