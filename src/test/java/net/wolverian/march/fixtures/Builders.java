package net.wolverian.march.fixtures;

import java.nio.file.*;

public final class Builders {
    private static final Path app = Paths.get("target/appassembler/bin/march").toAbsolutePath();

    private Builders() {}


    public static ProcessBuilder create() {
        String os = System.getProperty("os.name");

        if (os.startsWith("Windows")) {
            return new ProcessBuilder(app.toString());
        }

        ProcessBuilder posixStyleBuilder = new ProcessBuilder("/bin/sh", app.toString());

        if (os.equals("Mac OS X")) {
            return withJava7Home(posixStyleBuilder);
        }

        return posixStyleBuilder;
    }

    private static ProcessBuilder withJava7Home(ProcessBuilder builder) {
        builder.environment().put("JAVA_HOME", "/Library/Java/JavaVirtualMachines/1.7.0.jdk/Contents/Home");
        return builder;
    }
}
