package net.wolverian.march

import fixtures._
import Util._

import net.orfjackal.specsy._
import org.junit.runner.RunWith
import org.junit._
import org.junit.Assert._
import org.hamcrest.Matchers._
import java.io._
import com.google.common.io._

@RunWith(classOf[Specsy])
class CommandLineITSpec extends Spec {
  "A successful run" >> {
    "should build the artifact" >> {
      assertThat(builtFiles(project), hasItem(endsWith(".jar")))
    }

    "should not print anything to the error stream" >> {
      assertThat(process.getErrorStream.asText, is(""))
    }

    "should exit with successful exit code" >> {
      assertThat(process.exitValue, is(0))
    }
  }

  "Any run" >> {
    "should print a build started notification" >> {
      assertThat(process.getInputStream.asText, containsString("Build started"))
    }
  }

  @Rule val projects = new Projects
  val project = projects.createMavenProject()
  val process = {
    val process = Builders.create.directory(project.toFile).start()
    process.waitFor()
    process
  }

  implicit def richInputStream(inputStream: InputStream): RichInputStream = new RichInputStream(inputStream)

  class RichInputStream(inputStream: InputStream)  {
    def asText = CharStreams.toString(new InputStreamReader(inputStream))
  }
}